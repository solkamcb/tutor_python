from cv2 import bitwise_or, bitwise_xor, imread, ADAPTIVE_THRESH_MEAN_C, THRESH_BINARY, adaptiveThreshold, bilateralFilter, bitwise_and, cv2, cvtColor, destroyAllWindows, imshow, medianBlur, waitKey
import numpy as np

img = imread(r'Vitor2019.jpg')

gray = cvtColor(img, cv2.COLOR_BGR2GRAY)
gray = medianBlur(gray, 9)
edges = adaptiveThreshold(gray, 255, ADAPTIVE_THRESH_MEAN_C, THRESH_BINARY, 9, 9)

color = bilateralFilter(img, 9, 250, 250)
cartoon = bitwise_and(color, color, mask=edges)

imshow('Image', img)
imshow('edges', edges)
imshow('Cartoon', cartoon)
waitKey(0)
destroyAllWindows()

